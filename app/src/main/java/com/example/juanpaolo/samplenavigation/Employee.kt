package com.example.juanpaolo.samplenavigation

data class Employee(var name: String, var position: String)