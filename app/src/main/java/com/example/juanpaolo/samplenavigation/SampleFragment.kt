package com.example.juanpaolo.samplenavigation


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_sample.*


class SampleFragment : Fragment(), EmployeeAdapter.OnEmployeeClickListener {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sample, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        val emps = ArrayList<Employee>()
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))
        emps.add(Employee("Aldrin", "Android Dev"))
        emps.add(Employee("Paolo", "HomeBased iOS Dev"))
        emps.add(Employee("Vince", "Medyo Android Dev"))

        rvEmployees.layoutManager = LinearLayoutManager(activity)
        val adapter = EmployeeAdapter(activity!!, emps, this)

        rvEmployees.adapter = adapter
    }

    override fun onEmployeeClicked(employee: Employee) {
        Toast.makeText(activity, "Hi ${employee.name}", Toast.LENGTH_LONG).show()
    }


}
