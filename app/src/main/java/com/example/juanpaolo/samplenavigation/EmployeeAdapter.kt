package com.example.juanpaolo.samplenavigation

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.adapter_employee.view.*

class EmployeeAdapter(var context: Context,
                      var employees: ArrayList<Employee>,
                      var onEmployeeClickListener: OnEmployeeClickListener) : RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {

    interface OnEmployeeClickListener {
        fun onEmployeeClicked(employee: Employee)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val itemView = inflater.inflate(R.layout.adapter_employee, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return employees.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindEmployee(employees[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindEmployee(employee: Employee) {
            itemView.txtName.text = employee.name
            itemView.txtPosition.text = employee.position
            itemView.setOnClickListener {
                onEmployeeClickListener.onEmployeeClicked(employee)
            }
        }
    }
}